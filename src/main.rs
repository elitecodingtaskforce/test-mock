#![cfg_attr(feature = "nightly", feature(proc_macro))]

use std::time::{SystemTime, UNIX_EPOCH};

#[cfg(feature = "nightly")]
extern crate mock_derive;

#[cfg(feature = "nightly")]
use mock_derive::mock;

#[cfg_attr(feature = "nightly", mock)]
trait TimeGen {
    fn generate_time(&self) -> u64;
}

struct MyGen {}

impl MyGen {
    fn get_mygen_time(&self) -> u64 {
        let start = SystemTime::now();
        let since_the_epoch = start.duration_since(UNIX_EPOCH).unwrap();

        let in_ms = since_the_epoch.as_secs() * 1000 +
                since_the_epoch.subsec_nanos() as u64 / 1_000_000;
        in_ms
    }
}

impl TimeGen for MyGen {
    fn generate_time(&self) -> u64 {
        self.get_mygen_time()
    }
}


#[cfg(test)]
mod test {
    #[test]
    fn test_stable() {
        assert_eq!(1,1);
    }

}

#[cfg(all(test, feature = "nightly"))]
mod test_mocks {

    use super::*;

    #[test]
    fn test_mock() {
        let time_gen = MyGen{};

        let mut mock_time_gen = MockTimeGen::new();
        mock_time_gen.set_fallback(time_gen); // If a behavior isn't specified, we will fall back to this object's behavior.

        let method = mock_time_gen.method_generate_time()
            .first_call()
            .set_result(420);

        mock_time_gen.set_generate_time(method);

        // first call
        assert_eq!(mock_time_gen.generate_time(), 420);

        // second call shouldn't be mocked
        assert!(mock_time_gen.generate_time() != 420);
    }
}

fn main() {
    println!("hello");
}

